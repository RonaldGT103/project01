# Analytics / ML Models

### Identify

| Name                | Type                     | Version |
| ------------------- | ------------------------ | ------- |
| Nombre del proyecto | Modelo / POC / Prototipo | 1.0     |



### Authors
- Pedro Perez <pperez@example.com>

### Description
- Descripción del proyecto

### Quality metrics
- Accuracy = 0.99 on testing dataset

### Tecnologies
- **Tecnologias utilizadas y dependencias de software:** Frameworks, motores y librerias principales requeridas
- **Requerimientos de hardware:** Listado de hardware y/o componentes de red necesarios
- **Proyectos componentes:** Listado de enlaces a otras fichas de modelos o desarrollos requeridos

### Sources
- **Git repository:**  https://github.com/Azure/Azure-TDSP-ProjectTemplate
- **Model repository:**
	- Public URL:
	- Private URL (everis):
- **DVC repository:** Training and test datasets
- **Docker register:** https://hub.docker.com/_/ubuntu
- **Development image conf:** link to dokerfile for dev
- **Production image conf:** Link to dockerfile for prod or cert
- **Containers orchestation conf:** Link to docker compose or kubernetes configuration files

### Documentation
- **Use:**
	- **API:** http://localhost:7000/predict/ GET POST
	- **Dashboard:** http://localhost:7001/
	- **How to use:**
- **Presentations:**
- **Documentation:**
- **Architecture:**
- **Media:** 
- **Papers:**

### Uses
Descripción de posibles usos de esta tecnología o solución.

### TODOs

### Recomendations